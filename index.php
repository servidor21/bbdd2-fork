﻿<!DOCTYPE html>
   <html>
	<head>
	<style type="text/css">
	 a {
		text-align:center;
		font-family: Arial, Helvetica, sans-serif;
		font-size: 14px;
		text-decoration: none;
		color: #EE0000;
	}	
	</style>
        </head>
        
	<body>
	
<?php
require_once 'controlador/Alumno.php';
require_once 'vista/AlumnoVista.php';
require_once 'modelo/Usuario.php';
require_once 'vista/VistaUsuario.php';

abstract class Index
{
    public static function ejecutar()
    {
        session_start();
        if(!isset($_SESSION['usuario'])){
            //mostrar el login                    
            self::_login();
        }
        //si hay argumentos GET
        elseif(count($_GET)>0){
        // procesarModulo
            self::_procesarModulo();
        }
        //si no los hay llamar al moduloDefecto
        else{
            self::_moduloDefecto();
        }
    }    
    private static function _login()
    {
        //si hay post, validar
        if (isset($_POST['nombre']) && !empty($_POST)){
            $nombre=$_POST['nombre'];
            $password=$_POST['password'];
            if (Usuario::validar($nombre, $password)){
                self::_moduloDefecto();                
            }
            else{
                VistaUsuario::login("","", "Error de login. Inténtelo de nuevo");
            }
        }
        // si no hay post, formulario
        else{
            VistaUsuario::login();
        }
    }
    private static function _moduloDefecto()
    {
        echo "<h5>Usuario: $_SESSION[usuario]</h5>";        
        echo 'Pagina por Defecto'; 
        echo '<ul>'; 
        echo '<li><a href="?modulo=listado">Listado de alumnos</li>';
//        echo '<li><a href="?modulo=nuevoalumno">Crear nuevo alumno</li>';
        echo '</ul>';        
    }
    private static function _noExisteModulo()
    {
        echo 'El módulo solicitado no Existe';
        
        
        echo date() . '<br>';
        echo date('15/5/2016') . '<br>';
        
        $fecha =strtotime("now");
        echo date('d-m-Y', $fecha) . '<br>';
        $fecha =strtotime('5/15/2016');
        echo date('d-m-Y', $fecha) . '<br>';
        echo '<hr>';
        $fecha = DateTime::createFromFormat('j-M-Y', '15-Feb-2009');
        echo $fecha->format('Y-m-d') . '<br>';
        echo $fecha->format('d-m-Y'). '<br>';
        echo '<hr>';
        $fecha = DateTime::createFromFormat('d-m-Y', '25-04-2009');
        echo $fecha->format('Y-m-d') . '<br>';
        echo $fecha->format('d-m-Y'). '<br>';
        
        
        

//a) strtotime("now");
//b) date('d-m-Y', strtotime("now"));
//c) strtotime("27 September 1970");
//d) date('d-m-Y', strtotime("10 September 2000"));
//e) strtotime("+1 day");
//f) date('d-m-Y', strtotime("+1 day"));
//g) strtotime("+1 week");
//h) date('d-m-Y', strtotime("+1 week"));
//i) strtotime("+1 week 2 days 4 hours 2 seconds");
//j) date('d-m-Y', strtotime("+1 week 2 days 4 hours 2 seconds"));
//k) strtotime("next Thursday");
//l) date('d-m-Y', strtotime("next Thursday"));
//m) strtotime("last Monday");
//n) date('d-m-Y', strtotime("last Monday"));         
    }
    private static function _procesarModulo()
    {        
        switch($_GET['modulo']){
        case 'listado':                        
            $alumnos= Alumno::getAll();
            if(isset($_GET['mensaje'])){
                $mensaje=$_GET['mensaje'];                
            }
            else{
                $mensaje="";                
            }
            AlumnoVista::mostrarListado($alumnos,$mensaje);            
            break;
        case 'nuevoalumno':
            AlumnoVista::mostrarFormInsertar();
            break;
        case 'insertar':
            $alumno = new Alumno(null, $_POST['nombre'], $_POST['apellidos'], $_POST['edad'], $_POST['fechaMatricula']);
            if ($alumno->insertar()) {
                header("Location:index.php?modulo=listado&mensaje=insertado");
            }
            break;
        case 'borrar':
            $alumno = new Alumno;            
            $alumno->getAlumno($_GET['numero']);            
            if ($alumno->borrar()){
                header("Location:index.php?modulo=listado&mensaje=borrado");
            }
            break;
        case 'modificaralumno':
            $alumno= new Alumno;
            $alumno->getAlumno($_GET['numero']);
            AlumnoVista::mostrarFormModificar($alumno);
            break;
        case 'modificar': //alumnomodificado...
            $alumno = new Alumno;            
            $alumno->setNumero($_GET['numero']);
            $alumno->setNombre($_POST['nombre']);
            $alumno->setApellidos($_POST['apellidos']);
            $alumno->setEdad($_POST['edad']);            
            $alumno->setFechaMatricula($_POST['fechaMatricula']);            
            var_dump($alumno);

            if ($alumno->modificar()){
                header("Location:index.php?modulo=listado&mensaje=guardado");
            }
            break;
        case 'detalle':
            //recoger el ID
            $numero=$_GET['numero'];
            //crear un objeto y cargarlo de la bbdd
            $alumno=new Alumno;
            $alumno->getAlumno($numero);
            //mostrarlo
            AlumnoVista::mostrarDetalle($alumno);
            
            break;
        default :
            self::_noExisteModulo();
        }
    }
}


Index::ejecutar();