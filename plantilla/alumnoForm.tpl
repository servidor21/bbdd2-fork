<!DOCTYPE html>
<html>
    <head>
        <title>{$titulo}</title>

        <style type="text/css">
            a {
                text-align:center;
                font-family: Arial, Helvetica, sans-serif;
                font-size: 14px;
                text-decoration: none;
                color: #EE0000;
            }	
        </style>
    </head>

    <body>

        <h5>Usuario: {$usuario}</h5>
        <h1>{$titulo}</h1>
        <form action="?modulo={$accion}&numero=17" method="post">
            <input type='hidden' name='numero' value='{$numero}' readonly>
            Nombre: 
            <input type='text' name='nombre' value='{$nombre}'><br>
            Apellidos: 
            <input name='apellidos' value='{$apellidos}'><br>
            Edad: 
            <input name="edad" value="{$edad}"><br>
            Fecha Matrícula: 
            <input name="fechaMatricula" value="{$fechaMatricula}"><br>
            <input type='submit' name='sumbit' value='{$accion|capitalize}'>
        </form>
        <p><a href="?modulo=listado">Listado de alumnos</p>
    </body>
</html>