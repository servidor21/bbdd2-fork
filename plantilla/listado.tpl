<!DOCTYPE html>
<html>
    <head>
        <title>{$titulo}</title>
        <style type="text/css">
            a {
                text-align:center;
                font-family: Arial, Helvetica, sans-serif;
                font-size: 14px;
                text-decoration: none;
                color: #EE0000;
            }	
        </style>
    </head>

    <body>
        <h5>Usuario: {$usuario}</h5>
        <h1>{$titulo}</h1>
        {if $mensaje!=""}
        <h3>El alumno ha sido {$mensaje} correctamente </h3>
        {/if}
        <ul>
            {foreach from=$alumnos item=alumno}
{*            {foreach $alumnos as $alumno}*}
            <li>{$alumno.nombre} 
                <a href='?modulo=detalle&numero={$alumno.numero}'>Mostrar</a> |
                <a href='?modulo=modificaralumno&numero={$alumno.numero}'>Modificar</a> |
                <a href='?modulo=borrar&numero={$alumno.numero}'>Eliminar</a> |
            </li>
            {/foreach}
        </ul>
        <a href="?modulo=nuevoalumno">Nuevo Alumno</a>
</body>
</html>